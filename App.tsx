import React from 'react';
import NavigationContainer from './src/components/NavigationContainer/NavigationContainer';

function App(): JSX.Element {
  return <NavigationContainer />;
}

export default App;
