import React, {useLayoutEffect, useState} from 'react';
import {
  View,
  Switch,
  StyleSheet,
  useColorScheme,
  Appearance,
  Text,
} from 'react-native';

const Settings = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [isEnabled, setIsEnabled] = useState(!isDarkMode);

  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState);
  };

  useLayoutEffect(() => {
    Appearance.setColorScheme(!isEnabled ? 'dark' : 'light');
  }, [isEnabled]);


  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      backgroundColor: isDarkMode ? '#262625' : '#dedede',
      color: isDarkMode ? '#dedede' : '#262625',
    },
    text: {
      color: isDarkMode ? '#dedede' : '#262625',
    },
  });
  return (
    <View style={styles.container}>
      <Switch
        trackColor={{false: '#767577', true: '#81b0ff'}}
        thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
      <Text style={styles.text}>Modo {isEnabled ? 'Claro' : 'Oscuro'}</Text>
    </View>
  );
};

export default Settings;
