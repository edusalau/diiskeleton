import React from 'react';
import {StyleSheet, Button, View, useColorScheme} from 'react-native';
import {lightTheme, darkTheme} from '../../../theme';
import {ContentProps} from '../ContentView/ContentView';

type NavBarProps = {
  contents: ContentProps[];
  activeTab: string;
  onTabChange: (name: string) => void;
};

const NavBar = ({contents, activeTab, onTabChange}: NavBarProps) => {
  const theme = useColorScheme() === 'dark' ? darkTheme : lightTheme;
  const styles = StyleSheet.create({
    view: {
      flexDirection: 'row',
      height: 40,
    },
    button: {
      flex: 1,
      height: '100%',
    },
  });

  return (
    <View style={styles.view}>
      {contents.map((content, index) => {
        return (
          <View key={index} style={styles.button}>
            <Button
              title={content.name}
              color={
                activeTab === content.name
                  ? theme.PRIMARY_COLOR
                  : theme.SECONDARY_COLOR
              }
              onPress={() => onTabChange(content.name)}
            />
          </View>
        );
      })}
    </View>
  );
};

export default NavBar;
