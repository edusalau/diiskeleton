import React from 'react';
import {View, StyleSheet, useColorScheme} from 'react-native';

export type ContentProps = {
  name: string;
  content: any;
};

type ContentViewProps = {
  activeTab: string;
  contents: ContentProps[];
};

const ContentView = ({activeTab, contents}: ContentViewProps) => {
  const isDarkMode = useColorScheme() === 'dark';
  const styles = StyleSheet.create({
    content: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      width: '100%',
      backgroundColor: isDarkMode ? '#262625' : '#dedede',
      color: isDarkMode ? '#dedede' : '#262625',
    },
  });

  const currentContent = contents.find(element => element.name === activeTab);
  return (
    <View style={styles.content}>
      {currentContent && currentContent.content}
    </View>
  );
};

export default ContentView;
