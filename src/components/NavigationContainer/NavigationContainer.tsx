import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import NavBar from '../NavBar/NavBar';
import Settings from '../Settings/Settings';
import Main from '../Main/Main';
import List from '../List/List';

import ContentView from '../ContentView/ContentView';

export type ListProps = {
  name: string;
  image: string;
};

const NavigationContainer = () => {
  const [activeTabName, setActiveTabName] = useState('main');
  const handleActiveTabChange = (name: string) => {
    setActiveTabName(name);
  };
  const contents = [
    {name: 'settings', content: <Settings />},
    {name: 'main', content: <Main />},
    {name: 'list', content: <List />},
  ];
  return (
    <View style={{...styles.container}}>
      <NavBar
        activeTab={activeTabName}
        contents={contents}
        onTabChange={handleActiveTabChange}
      />
      <ContentView activeTab={activeTabName} contents={contents} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
    aspectRatio: 1,
  },
});

export default NavigationContainer;
