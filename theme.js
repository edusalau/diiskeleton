const darkTheme = {
  PRIMARY_COLOR: '#c98a02',
  SECONDARY_COLOR: '#a18d64',
  TITLE_COLOR: '#dedede',
  BACKGROUND_COLOR: '#262625',
};
const lightTheme = {
  PRIMARY_COLOR: '#398c06',
  SECONDARY_COLOR: '#658a4e',
  TITLE_COLOR: '#262625',
  BACKGROUND_COLOR: '#dedede',
};
export {lightTheme, darkTheme};
